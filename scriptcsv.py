import csv
import datetime

#Initialisation des noms fichiers et des delimiteurs
delimiteur1 = '|'   
delimiteur2 = ';'
source_fichier = 'old_auto.csv'
dest_fichier = 'new_auto.csv'

#Reception Fichier
fichier = open(source_fichier, 'r', newline='')

#Initialisation des Nom catégories
#Attribution des nouveaux noms aux catégories RENOMMAGE / Liaison valeurs aux noms (DICTIONNAIRE)
colonnes_dict={
    'address': 'adresse_titulaire',
    'carrosserie': 'carrosserie',
    'categorie': 'categorie',
    'couleur': 'couleur',
    'cylindree': 'cylindree',
    'date_immat': 'date_immatriculation',
    'denomination': 'denomination_commerciale',
    'energy': 'energie',
    'firstname': 'prenom',
    'immat': 'immatriculation',
    'marque': 'marque',
    'name': 'nom',
    'places': 'places',
    'poids': 'poids',
    'puissance': 'puissance',
    'type_variante_version': 'type',
    'vin': 'vin',
}
#Ordre Catégorie
colonnes_modif = ['adresse_titulaire','nom','prenom','immatriculation','date_immatriculation','vin','marque','denomination_commerciale','couleur','carrosserie','categorie','cylindree','energie','places','poids','puissance','type','variante','version']

#Ouverture / Création / Formatage d'un fichier auto2
fichier2 = open(dest_fichier, 'w', newline='')

#Attribution des nouvelles colonnes dans l'ordre + changement des délimiteurs
fichier_w = csv.DictWriter(fichier2, fieldnames=colonnes_modif, delimiter=delimiteur2 )
#Ecrit la ligne des colonnes
fichier_w.writeheader()

#Lecture avec le delimiteur '|'
fichier_r = csv.DictReader(fichier, delimiter=delimiteur1)



for ligne in fichier_r :
    ligne1 = {}
    for indiceP in ligne.keys() : #Parcours des colonnes
        ligne1[colonnes_dict[indiceP]] = ligne[indiceP]
        if indiceP == 'type_variante_version':#Division Catégorie type_variante_version
            tvv_stock = ligne[indiceP].split(', ') #split via le délimiteur ', '
            ligne1['type'] = tvv_stock[0]
            ligne1['variante'] = tvv_stock[1]
            ligne1['version'] = tvv_stock[2]
        if indiceP == 'date_immat':
            ligne1['date_immatriculation'] = datetime.datetime.strptime(ligne[indiceP], '%Y-%m-%d').strftime('%d/%m/%y') #Conversion Format heure de Y-M-D en D-M-Y
    ligne={}
    #attribue les valeurs aux colonnes
    for fieldname in colonnes_modif:
        ligne[fieldname] = ligne1[fieldname]
    #Ecrit la ligne courante
    fichier_w.writerow(ligne)



#A FAIRE : 
# DELIMITEUR EN PARAMETRE DU SCRIPT
#METTRE DANS DES FONCTIONS DIFFERENTES